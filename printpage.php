



<?php

/**
 * Contains the Print page view for Marking guide 
 
 * TODO - convert plain HTML/PHP to Moodle html::write 
          pagination ,Styling and More UI Changes This can be improved hevily with time 
 * @package    gradingform_acbc
 * @copyright  2014 ecreators     Thushara Ranepura  Email:thushara@ecreators.com.au
 * 
 */

require_once('../../../../config.php');
require_once('lib.php');

?>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0; padding :20px; margin:5px; width:100%;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.adjuster{
    padding:20px;
}
.lock-logo{
    padding:5px;
    margin:4px;
}
.scalermiddle{width:20%}
.scalerleft{width:40%}
.scalerright{width:40%}
.satisscaler{padding:10px; margin:10px;}
</style>

<?php

require_login();
$pageurl = new moodle_url('/');
global $CFG;


$locklogo = '<div class="lock-logo"><img src="'.$CFG->wwwroot.'/grade/grading/form/acbc/pix/acbclogo.png" width="200px" height="54px" /></div>';

$context = get_context_instance(CONTEXT_SYSTEM);


 $PAGE->set_context($context);

$criteriaid = required_param('idcriteria',PARAM_INT); 
$instanceid = required_param('instanceid',PARAM_INT);
$courseid = required_param('rid',PARAM_INT);
$mode = required_param('mode',PARAM_INT);
$assessorname = optional_param('assessorname','',PARAM_INT);
//echo $criteriaid.$instanceid.'course';



$criteriarecord = $DB->get_records('gradingform_acbc_criteria',array('definitionid'=>$criteriaid));

$instancerecord = $DB->get_records('gradingform_acbc_fillings',array('instanceid'=>$instanceid));

$course = $DB->get_record('course',array('id'=>$courseid));


//var_dump($criteriarecord);

//var_dump($instancerecord);

$holder ='';
$commentholder='';

foreach ($instancerecord as $value) {
    
   $holder = $value->satisfactory;
   $commentholder = $value->remark;
}




echo $locklogo;


echo '<table  class="tg">';

  echo '<tr>';
 echo   '<td class="tg-031e" colspan="3" rowspan="2"> <b> Course Name:</b> '.$course->fullname.'  </td>';
  echo '</tr>';
 
  echo '<tr>';
  echo '</tr>';

  echo  '<tr>';
  echo  ' <td class="tg-031e" colspan="3"><b> RTO:<br></b>  </td>';
  echo '</tr>';
  
  
 echo  '<tr>';
  echo  ' <td class="tg-031e" colspan="3"><b> Student Name:<br></b>  </td>';
  echo '</tr>';
 
  echo '<tr>';
   echo  '<td class="tg-031e" colspan="3"><b>Assessor Name :<br> </b></td>';
  echo '</tr>';
  
  echo '<tr>';
  echo   '<td class="tg-031e" colspan="3"><b>Unit Marking Guide For <br> </b></td>';
  echo '</tr>';



echo '<tr>';
echo   '<th class="scalerleft"> <b> In Undertaking this assessment,did the <br> student  undertake the following: </b></th>';
 echo ' <th class="scalermiddle"> <b> Satisfactory </b> </th>';
  echo '<th> <b> Comments </b> </th>';
 echo ' </tr>';
    
    
    
    
    
foreach ($criteriarecord as $key ) {
        
   echo '<tr>';
   echo '<td>';
    echo  '<b>'.$key->shortname.'</b><br>'; 
    
    echo $key->descriptionmarkers.'<br>' ; 
    
    echo '</td>';
    //echo $criteriatable;
   
    if($mode==1){
    echo ' <td class="satisscaler">&nbsp;&nbsp;&nbsp;&nbsp; &#x25a1; Yes &nbsp;&nbsp; &#x25a1; No </td>';
    }
    if($mode==2){
        
        if(empty($holder)){
            $holder ='Satisfactory not set yet!';
        }
    echo ' <td class="satisscaler">&nbsp;&nbsp;&nbsp;&nbsp; '.$holder .'</td>';
    }
   
   if($mode==1){
   echo ' <td>  </td>';
   }
   
   if($mode==2){
     
        if(empty($commentholder)){
            $commentholder ='No comments added yet!';
        }
       
       
    echo ' <td> &nbsp;&nbsp;&nbsp;&nbsp;'. $commentholder .'</td>';   
   }
   
   
   echo '</tr> ';
 
  }
  
 //echo '<tr>';
 // echo   '<td class="tg-031e" colspan="3"><b>Feedback: <br> </b></td>';
 // echo '</tr>';
  
  echo '<tr>';
  echo   '<td class="tg-031e" colspan="3">
      
       <div class="adjuster" >
       <b  >Student signature:&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95 <br></b>
       </div>  

      <div class="adjuster" >
      <b  >Assessor signature:&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95 <br> </b>
       </div>  
       
       <div class="adjuster" >
       <b >Date:&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95&#95 <br> </b>
       </div> '; 
  echo '</td>';
  
  echo '</tr>';
  
  echo '</table>';
  
  
  $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
  
  
  
  $modinfo = get_fast_modinfo($course);
    $modnames = get_module_types_names();
    $modnamesplural = get_module_types_names(true);
    $modnamesused = $modinfo->get_used_module_names();
    $mods = $modinfo->get_cms();
    $sections = $modinfo->get_section_info_all();

  
 //print_r($modnamesused);
 
 $renderer = $PAGE->get_renderer('core','course');   
 
 //print_r($renderer );
    
 
 